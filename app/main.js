/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  View,
  StatusBar
} from 'react-native';
import { 
  Container, 
  Header,
  Title,
  Left,
  Body,
  Right,
  Button,
  Icon,
  Tab, 
  Tabs, 
  TabHeading, 
  Text } from 'native-base';
import Tab1 from './restaurantlist';
import Tab2 from './orders';
import Tab3 from './account';

export default class TopTabs extends Component {
  render() {
    return (
      <Container style={{backgroundColor: '#888'}}>
        <Header hasTabs>
          <Left/>
          <Body>
            <Title>CC Food</Title>
          </Body>
          <Right>
            <Button transparent>
              <Icon name='search' />
            </Button>
            <Button transparent>
              <Icon name='cart' />
            </Button>
            <Button transparent>
              <Icon name='more' />
            </Button>
          </Right>
        </Header>
        <Tabs>
          <Tab heading={ <TabHeading><Text>Restaurants</Text></TabHeading>}>
            <Tab1 />
          </Tab>
          <Tab heading={ <TabHeading><Text>Orders</Text></TabHeading>}>
            <Tab2 />
          </Tab>
          <Tab heading={ <TabHeading><Text>Account</Text></TabHeading>}>
            <Tab3 />
          </Tab>
        </Tabs>
      </Container>
    );
  }
}