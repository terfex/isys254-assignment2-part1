
import React, { Component } from "react";
import { 
    Container, 
    Header, 
    Title, 
    Content,
    View,
    Button, 
    Icon, 
    Left, 
    Right, 
    Body,
    List, 
    ListItem, 
    Text 
} from "native-base";
export default class HeaderSpan extends Component {
  render() {
    return (
      <Container>
        <Header span>
            <Left>
              <Button transparent>
                <Icon name='arrow-back' />
              </Button>
            </Left>
            <Body>
                <Title>Your Order</Title>
            </Body>
            <Right />
        </Header>
        <Content padder>
          <List style={{paddingBottom: 50}}>
            <ListItem avatar>
              <Body>
                <Text>Meat Lovers</Text>
                <Text note>Standard toppings, pan base</Text>
              </Body>
              <Right>
                <Text note>$15.00</Text>
              </Right>
            </ListItem>
            <ListItem avatar>
              <Body>
                <Text style={{fontSize: 20}}>TOTAL</Text>
              </Body>
              <Right>
                <Text style={{fontSize: 20}}>$15.00</Text>
              </Right>
            </ListItem>
          </List>
          <Button block style={{backgroundColor: '#00E676'}}>
            <Text>Pay Now</Text>
          </Button>
        </Content>
      </Container>
    );
  }
}