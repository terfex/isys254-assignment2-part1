
import React, { Component } from "react";
import { 
    Container, 
    Header, 
    Title, 
    Content,
    View,
    Button, 
    Icon, 
    Left, 
    Right, 
    Body, 
    Text 
} from "native-base";
export default class HeaderSpan extends Component {
  render() {
    return (
      <Container>
        <Header span>
            <Left/>
            <Body>
                <Title>Order Confirmed</Title>
            </Body>
            <Right>
                <Button transparent>
                    <Icon name='md-close' />
                </Button>
            </Right>
        </Header>
        <Content padder>
          <View style={{flex: 1, alignSelf: "stretch", justifyContent: "center", padding: 20}}>
            <Icon ios='ios-checkmark-circle-outline' android="md-checkmark-circle-outline" style={{fontSize: 150, color: '#00E676', textAlign: 'center', padding: 20}}/> 
            <Text style={{fontSize: 20, textAlign: 'center', paddingTop: 20, paddingBottom: 10}}>
              Your order has been confirmed
            </Text>
            <Text style={{fontSize: 15, textAlign: 'center', paddingTop: 10}}>
              Your order number is: NUMBER
            </Text>
            <Text style={{fontSize: 15, textAlign: 'center', paddingTop: 10, paddingBottom: 50}}>
              Estimated pickup time is 00:00 PM
            </Text>
            <Button block style={{backgroundColor: '#00E676'}}>
              <Text>OK</Text>
            </Button>
          </View>
        </Content>
      </Container>
    );
  }
}