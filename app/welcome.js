/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  ImageBackground,
  Image,
  StatusBar
} from 'react-native';
import { 
  Button 
} from 'react-native-elements'


type Props = {};
export default class App extends Component<Props> {
  render() {
    return (
      <ImageBackground source={require('./splashscreen.jpg')} style={styles.container} imageStyle={{ resizeMode: 'cover' }}>
        <StatusBar
          barStyle='light-content'
          translucent={true}
          backgroundColor={'transparent'}
        />
        <View>
          <Text style={styles.user}>
            Logged in as USER. Logout?
          </Text>
          <Text style={styles.title}>
            Campus Common Food
          </Text>
          <Text style={styles.welcome}>
            Welcome, press Order Food to start.
          </Text>
        </View>
        <View	style={styles.button}>
          <Button
            large
            title='Order Food'
            backgroundColor='#00E676'
          />
        </View>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  user: {
    fontSize: 15,
    textAlign: 'right',
    color: '#ffffff',
    marginTop: 30,
  },
  title: {
    fontSize: 38,
    textAlign: 'center',
    color: '#ffffff',
    marginTop: 38,
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    color: '#ffffff',
    marginTop: 30,
  },
  button:{
    alignItems:	'center',
    justifyContent:	'center',
    marginBottom:	50,
  }
});
