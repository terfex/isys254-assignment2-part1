/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image
} from 'react-native';
import { 
  Avatar,
  Button
} from 'react-native-elements'


type Props = {};
export default class App extends Component<Props> {
  render() {
    return (
      <View>
        <View style={styles.image_view}>
          <Avatar style={styles.avatar}
          xlarge
          rounded
          source={{uri: "https://s3.amazonaws.com/uifaces/faces/twitter/brynn/128.jpg"}}
          activeOpacity={0.7}
          />
        </View>
        <View>
          <Text style={styles.name}>
            User
          </Text>
        </View>
        <View	style={styles.button}>
          <Button
            large
            title='Logout'
            backgroundColor='#F44336'
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  image_view: {
    alignItems: 'center',
    marginTop: 10,
    marginBottom: 10,
  },
  name: {
    fontSize: 38,
    textAlign: 'center',
    marginTop: 30,
  },
  button:{
    alignItems:	'center',
    justifyContent:	'center',
    marginTop:	30
  }
});
