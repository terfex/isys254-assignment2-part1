
import React, { Component } from 'react';
import { 
  Image 
} from 'react-native';
import { 
  Container, 
  Content, 
  Card, 
  CardItem, 
  Thumbnail, 
  Text, 
  Button, 
  Icon, 
  Left,
  Right, 
  Body 
} from 'native-base';
export default class RestaurantList extends Component {
  render() {
    return (
      <Container>
        <Content>
          <Card style={{flex: 0}}>
            <CardItem>
              <Left>
                <Body>
                  <Text>Mario's</Text>
                  <Text>Italian</Text>
                </Body>
              </Left>
            </CardItem>
            <CardItem>
              <Body>
                <Image source={require('./italian.jpg')} style={{height: 200, width: 375, flex: 1}}/>
                <Text>
                  Authentic wood-fired pizza and freshly made pasta. Bring your friends along for a great time.
                </Text>
              </Body>
            </CardItem>
            <CardItem>
              <Left>
                <Button transparent textStyle={{color: '#87838B'}}>
                  <Icon name="md-thumbs-up" />
                  <Text>1,926 likes</Text>
                </Button>
              </Left>
              <Right>
                <Button style={{backgroundColor: '#00E676'}}>
                  <Text>Order Now</Text>
                </Button>
              </Right>
            </CardItem>
          </Card>
          <Card style={{flex: 0}}>
            <CardItem>
              <Left>
                <Body>
                  <Text>Nacho Libre</Text>
                  <Text>Mexican</Text>
                </Body>
              </Left>
            </CardItem>
            <CardItem>
              <Body>
                <Image source={require('./mexican.jpg')} style={{height: 200, width: 375, flex: 1}}/>
                <Text>
                  Wholesome burritos with the unmistakable taste of Mexico.
                </Text>
              </Body>
            </CardItem>
            <CardItem>
              <Left>
                <Button transparent textStyle={{color: '#87838B'}}>
                  <Icon name="md-thumbs-up" />
                  <Text>954 likes</Text>
                </Button>
              </Left>
              <Right>
                <Button style={{backgroundColor: '#00E676'}}>
                  <Text>Order Now</Text>
                </Button>
              </Right>
            </CardItem>
          </Card>
          <Card style={{flex: 0}}>
            <CardItem>
              <Left>
                <Body>
                  <Text>Golden Palace</Text>
                  <Text>Chinese</Text>
                </Body>
              </Left>
            </CardItem>
            <CardItem>
              <Body>
                <Image source={require('./chinese.jpg')} style={{height: 200, width: 375, flex: 1}}/>
                <Text>
                  Treat your taste buds with authentic Chinese food at the Golden Palace.
                </Text>
              </Body>
            </CardItem>
            <CardItem>
              <Left>
                <Button transparent textStyle={{color: '#87838B'}}>
                  <Icon name="md-thumbs-up" />
                  <Text>2,654 likes</Text>
                </Button>
              </Left>
              <Right>
                <Button style={{backgroundColor: '#00E676'}}>
                  <Text>Order Now</Text>
                </Button>
              </Right>
            </CardItem>
          </Card>
        </Content>
      </Container>
    );
  }
}