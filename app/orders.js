import React, { Component } from 'react';
import { 
  Container, 
  Header, 
  Content, 
  List, 
  ListItem, 
  Text, 
  Separator 
} from 'native-base';
export default class ListSeparatorExample extends Component {
  render() {
    return (
      <Container>
        <Content>
          <Separator bordered>
            <Text>UPCOMING</Text>
          </Separator>
          <ListItem >
            <Text>Golden Palace</Text>
          </ListItem>
          <Separator bordered>
            <Text>PAST</Text>
          </Separator>
          <ListItem>
            <Text>Mario's</Text>
          </ListItem>
        </Content>
      </Container>
    );
  }
}